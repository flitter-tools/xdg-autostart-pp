CC?=gcc

CFLAGS?=-g -O2 -Wall
LDFLAGS?=

xdg-autostart-pp: xdg-autostart-pp.o

xdg-autostart-pp.o: xdg-autostart-pp.c

install: xdg-autostart-pp
	mkdir -p $(DESTDIR)/usr/bin
	cp -f xdg-autostart-pp $(DESTDIR)/usr/bin
	chmod 755 $(DESTDIR)/usr/bin/xdg-autostart-pp

clean:
	rm -f xdg-autostart-pp *.o
