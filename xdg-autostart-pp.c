#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

char *getField(char *filepath, char *token)
{
	char *line, *tmp, *tmp1 = NULL;
	size_t len = 0;
    ssize_t read;
		
	FILE *fp;
	fp = fopen(filepath, "r");
	
	if (fp) 
	{
		while ((read = getline(&line, &len, fp)) != -1) 
		{
			tmp = tmp1 = NULL;
			
			if (strcmp(&line[0], "#") == 0)
				continue;
			
			tmp = strtok(line, "=");
			if (strcmp(tmp, token) == 0)
			{
				tmp1 = strtok(NULL, "=");
				strtok(tmp1, "\n"); // remove tailing \n
				break;
			}
		}

		fclose(fp);
	}

	return tmp1;
}

void autostart(char *xdg_dir)
{
	DIR *d;
	struct dirent *dir;
	char buf[255];
	char *buf_exec, *buf_nodisplay = NULL;
	pid_t pid;
	
	d = opendir(xdg_dir);
	
	if (d) 
	{
		while ((dir = readdir(d)) != NULL)
		{
			char *dot = strrchr(dir->d_name, '.');
			if (dot && !strcmp(dot, ".desktop"))
			{
				memcpy(buf, xdg_dir, strlen(xdg_dir));
				memcpy(buf + strlen(xdg_dir), dir->d_name, strlen(dir->d_name) + 1);
								
				buf_nodisplay = getField(buf, "NoDisplay");
				
				if (buf_nodisplay && !strcmp(buf_nodisplay, "true"))
					continue;
				
				buf_exec = getField(buf, "Exec");
				if (buf_exec)
				{
					printf("Execute %s\n", buf_exec);
					
					if ((pid = fork()) < 0)
						continue;
					else if (pid == 0)
						execlp(buf_exec, buf_exec, (char*)NULL);
				}
			}
		}
		
		closedir(d);
	}
}

int main(int argc, char **argv)
{
	char buf[255];
	char *HOME = getenv("HOME");
	char *autostarthome = "/.config/autostart/";
	
	memcpy(buf, HOME, strlen(HOME));
	memcpy(buf + strlen(HOME), autostarthome, strlen(autostarthome) + 1);
	
	char *xdg_path[2] = {"/etc/xdg/autostart/", buf};
	
	for (int i = 0; i < 2; i++)
		autostart(xdg_path[i]);
	
	return 0;
}
